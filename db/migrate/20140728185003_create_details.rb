class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.string :thc
      t.string :cbd
      t.string :cbn
      t.references :strain, index: true

      t.timestamps
    end
  end
end
