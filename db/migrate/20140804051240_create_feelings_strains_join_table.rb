class CreateFeelingsStrainsJoinTable < ActiveRecord::Migration
  def self.up
    create_table :feelings_strains, :id => false do |t|
      t.integer :feeling_id
      t.integer :strain_id
    end

    add_index :feelings_strains, [:feeling_id, :strain_id]
  end

  def self.down
    drop_table :feelings_strains
  end
end
