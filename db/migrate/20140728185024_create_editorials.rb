class CreateEditorials < ActiveRecord::Migration
  def change
    create_table :editorials do |t|
      t.string :opinion
      t.references :strain, index: true

      t.timestamps
    end
  end
end
