# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140804051240) do

  create_table "details", force: true do |t|
    t.string   "thc"
    t.string   "cbd"
    t.string   "cbn"
    t.integer  "strain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "details", ["strain_id"], name: "index_details_on_strain_id"

  create_table "editorials", force: true do |t|
    t.string   "opinion"
    t.integer  "strain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "editorials", ["strain_id"], name: "index_editorials_on_strain_id"

  create_table "feelings", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feelings_strains", id: false, force: true do |t|
    t.integer "feeling_id"
    t.integer "strain_id"
  end

  add_index "feelings_strains", ["feeling_id", "strain_id"], name: "index_feelings_strains_on_feeling_id_and_strain_id"

  create_table "strains", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
