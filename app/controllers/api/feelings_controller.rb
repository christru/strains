module Api
  class FeelingsController < ApplicationController
    respond_to :json

    def index
      respond_with(@feelings = Feeling.all)
    end

  end
end
