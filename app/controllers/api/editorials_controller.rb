module Api
  class EditorialsController < ApplicationController
    before_action :set_strain, only: [:show, :edit, :update, :destroy]
    respond_to :json

    def index
      respond_with @strain.editorial
    end

    def create
      respond_with @strain.create_editorial(strain_params)
    end

    def update
      respond_with @strain.editorial.update(strain_params)
    end

    def destroy
      respond_with @strain.editorial.destroy
    end

    def show
      respond_with @strain.editorial
    end

    private
    def set_strain
      @strain = Strain.find(params[:strain_id])
    end

    def strain_params
      params.require(:editorial).permit(:strain_id, :opinion)
    end
  end
end
