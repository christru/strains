module Api
  class DetailsController < ApplicationController
    respond_to :json
    before_action :set_strain, only: [:index, :show, :edit, :update, :destroy]

    def index
      respond_with @strain.detail
    end

    def create
      respond_with @strain.create_detail(strain_params)
    end

    def update
      respond_with @strain.detail.update(strain_params)
    end

    def destroy
      respond_with @strain.detail.destroy
    end

    def show
      respond_with @strain.detail
    end

    private
    def set_strain
      @strain = Strain.find(params[:strain_id])
    end

    def strain_params
      params.require(:detail).permit(:strain_id, :thc, :cbd, :cbn)
    end
  end
end
