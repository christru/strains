module Api
  class StrainsController < ApplicationController
    respond_to :json
    before_action :set_strain, only: [:show, :edit, :update, :destroy]
    #so i dont have to wrap what i send. stupid rails
    wrap_parameters :strain, include: [:id, :name, :feeling_ids, :detail_attributes, :editorial_attributes, :feelings]

    def index
      respond_with(@strains = Strain.all)
    end

    def create
      respond_with Strain.create(strain_params)
    end

    def update
      respond_with @strain.update(strain_params)
    end

    def destroy
      respond_with @strain.destroy
    end

    #testing
    def show
      respond_with @strain
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_strain
      @strain = Strain.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    #need to check into the way i create strain object with details.
    def strain_params
      params.require(:strain).permit(:id, :name, :feeling_ids => [], detail_attributes:[:id, :thc, :cbd, :cbn], editorial_attributes:[:id, :opinion ])
    end
  end
end
