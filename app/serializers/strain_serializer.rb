class StrainSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_one :editorial
  has_one :detail
  has_many :feelings
end
