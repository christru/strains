class EditorialSerializer < ActiveModel::Serializer
  attributes :id, :opinion
end
