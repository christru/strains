angular.module('app.services', [])

    .service('strainService', ['$http', function ($http) {

//send back the promise deal with it in the controller instead.

        var url = '/api/strains';

        this.getStrains = function () {
            return $http.get(url);
        };

        this.getStrain = function (id) {
            return $http.get(url + '/' + id);
        };

        this.createStrain = function (strn) {
            return $http.post(url, strn);
        };

        this.updateStrain = function (strn) {
            return $http.put(url + '/' + strn.id, strn)
        };

        this.deleteStrain = function (id) {
            return $http.delete(url + '/' + id);
        };

        this.getDetail = function (id) {
            return $http.get(url + '/' + id + '/detail');
        };

        this.getEditorial = function (id) {
            return $http.get(url + '/' + id + '/editorial')
        }

    }])

    .service('feelingService', ['$http', function ($http) {

        var url = 'api/feelings';
        this.getFeelings = function () {
            //set accept to */* for some reason was requesting as html and having an issue.
            return $http.get(url, {headers: {'Accept': '*/*'}});
        }

    }]);
