angular.module('app.controllers', ['app.services'])

//show all strains
    .controller('StrainsController', function ($scope, $location, strainService) {
        getStrains();
        $scope.alerts = [];

        function getStrains() {
            strainService.getStrains()
                .success(function (strains) {
                    $scope.strains = strains;
                })
                .error(function (error) {
                    $scope.status = error.message;
                });
        }

//destroy strain
        $scope.destroy = function (id, index) {
            strainService.deleteStrain(id)
                .success(function () {
                    $scope.status = 'Delete success!';
                    $scope.strains.splice(index, 1);
                    $scope.alerts.push({type: 'success', msg: 'strain deleted!'});
                })
                .error(function (error) {
                    $scope.status = error.message;
                    $scope.alerts.push({type: 'error', msg: error.message });
                });
        };

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

    })

//view one strain
    .controller('StrainViewController', function ($scope, $routeParams, $location, strainService) {
        show();
        function show() {
            strainService.getStrain($routeParams.id)
                .success(function (strain) {
                    $scope.strain = strain;
                })
                .error(function (error) {
                    $scope.status = error.message;
                    console.log(error);
                });
        }

//destroy strain
        $scope.destroy = function (id) {
            strainService.deleteStrain(id)
                .success(function () {
                    $scope.status = 'Delete success!';

                })
                .error(function (error) {
                    $scope.status = error.message;
                });
        }
    })

//edit strain
    .controller('StrainEditController', function ($scope, $location, $routeParams, strainService,feelingService) {
        $scope.strain = [];
        $scope.alerts = [];
        $scope.feeling_ids = [];
        show();
        getFeelings();


        function show() {
            strainService.getStrain($routeParams.id)
                .success(function (data) {
                    $scope.strain = data;
                    $scope.strain.editorial_attributes = $scope.strain.editorial;
                    $scope.strain.detail_attributes = $scope.strain.detail;

                    angular.forEach($scope.strain.feelings, function(obj){
                        //cant push array only object?
                      $scope.feeling_ids.push(obj.id);
                    });

                   $scope.strain.feeling_ids = angular.copy($scope.feeling_ids);

                    delete $scope.strain.editorial;
                    delete $scope.strain.detail;
                    delete $scope.strain.feelings;


                })
                .error(function (error) {
                    $scope.status = error.message;
                    $scope.alerts.push({type: 'error', msg: error.message });
                    console.log(error);
                });
        }

        function getFeelings() {
            feelingService.getFeelings()
                .success(function (feelings) {
                    $scope.feelings = feelings;
                })
        }

//update strain
        $scope.update = function () {
            strainService.updateStrain($scope.strain)
                .success(function () {
                    $scope.status = "Update success!";
                    $location.path('/');
                })
                .error(function (error) {
                    $scope.status = error.message;
                });
        };

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
    })

//create strain
    .controller('StrainCreateController', function ($scope, $location, $routeParams, strainService, feelingService) {
        $scope.levels =
            [
                "low",
                "medium",
                "high"
            ];

        getFeelings();


        function getFeelings() {
            feelingService.getFeelings()
                .success(function (feelings) {
                    $scope.feelings = feelings;
                })
        }

        $scope.save = function () {
            strainService.createStrain($scope.strain)
                .success(function () {
                    $scope.status = 'Create success!';
                    $location.path('/');
                    //$scope.strains.push($scope.strain);
                }).
                error(function (error) {
                    $scope.status = error.message;
                    $location.path('/');
                });
        }
    });