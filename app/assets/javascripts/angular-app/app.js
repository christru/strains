var app = angular.module('strainApp', ['ngRoute', 'ngResource', 'ui.bootstrap', 'app.services', 'app.controllers']);

app.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: '/list',
        controller: 'StrainsController'
    })
        .when('/strains/new', {
            templateUrl: '/new',
            controller: 'StrainCreateController'
        })
        .when('/strains/:id/edit', {
            templateUrl: '/edit',
            controller: 'StrainEditController'
        })
        .when('/strains/:id/show', {
            templateUrl: '/show',
            controller: 'StrainViewController'
        })
        .otherwise({ redirectTo: '/'});
});




