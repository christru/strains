class Strain < ActiveRecord::Base
  has_one :detail, :dependent => :destroy
  has_one :editorial, :dependent => :destroy
  has_and_belongs_to_many :feelings
  accepts_nested_attributes_for :detail
  accepts_nested_attributes_for :editorial
end
